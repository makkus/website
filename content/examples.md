+++
title = "Examples"
description = "Example use-cases for freckles"
keywords = ["Examples"]
+++

The examples listed here only contain the minimal amount of configuration necessary. For a more complete configuration including ``args`` keys etc. check out the 'More details' link in every one of the items.

## Host a static website

### Without https certificate

Config:

``` yaml
tasks:
  - generate
  - install-nginx
```

Command:

``` shell
frecklecute asdf
```

### With https certificate from [LetsEncrypt](https://letsencrypt.org/)


---

> In case you haven't found the answer for your question please feel free to contact us, our customer support will be happy to help you.
